package ru.karamyshev.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IAdminUserEndpoint;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws Exception {
        userService.create(login, password, email);

    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role
    ) throws Exception {
        sessionService.validate(session, Role.ADMIN);
        userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public void lockUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        sessionService.validate(session);
        @Nullable final User currentUser = userService.findById(session.getUserId());
        userService.lockUserByLogin(currentUser.getLogin(), login);
    }

    @Override
    @WebMethod
    public void unlockUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        sessionService.validate(session);
        @Nullable final User currentUser = userService.findById(session.getUserId());
        userService.unlockUserByLogin(currentUser.getLogin(), login);
    }

    @Override
    @WebMethod
    public void removeUsByLog(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        sessionService.validate(session);
        userService.removeByLogin(login);
    }

}
