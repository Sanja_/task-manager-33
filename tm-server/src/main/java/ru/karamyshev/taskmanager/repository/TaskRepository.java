package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Nullable
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        @Nullable final TypedQuery<Task> typedQuery = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId", Task.class);
        typedQuery.setParameter("userId", userId);
        if (typedQuery == null) return null;
        return typedQuery.getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @Nullable final TypedQuery<Task> typedQuery = entityManager.createQuery(
                "WHERE FROM Task", Task.class);
        if (typedQuery == null) return null;
        return typedQuery.getResultList();
    }

    @Override
    public void clear(final @NotNull String userId) {
        entityManager.createQuery(
                "DELETE Task WHERE user_id = :userId")
                .setParameter("userId",userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TypedQuery<Task> typedQuery = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId AND id = :id", Task.class);
        typedQuery.setMaxResults(1);
        typedQuery.setParameter("userId", userId);
        typedQuery.setParameter("id", id);
        if (typedQuery == null) return null;
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        entityManager.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final TypedQuery<Task> typedQuery = entityManager.createQuery(
                "FROM Task WHERE user_id = :userId AND name = :name", Task.class);
        typedQuery.setMaxResults(1);
        typedQuery.setParameter("userId", userId);
        typedQuery.setParameter("name", name);
        if (typedQuery == null) return null;
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @NotNull final String name) {
        final Task tasks = findOneByName(userId, name);
        if (tasks == null) return null;
        entityManager.remove(tasks);
        return tasks;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task", Task.class)
                .executeUpdate();
    }

}
