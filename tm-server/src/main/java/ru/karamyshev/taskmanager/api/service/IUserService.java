package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.util.List;

public interface IUserService {

    @Nullable User findById(@Nullable String id) throws Exception;

    @Nullable User findByLogin(@Nullable String login) throws Exception;

    @Nullable List<UserDTO> findAll();

    @Nullable void removeUser(@Nullable User user) throws Exception;

    @Nullable void removeById(@Nullable String id) throws Exception;

    @Nullable void removeByLogin(@Nullable String login) throws Exception;

    @Nullable void removeUserByLogin(
            @Nullable String currentLogin,
            @Nullable String login
    );

    void removeAllUsers();

    @Nullable void create(
            @Nullable final String login,
            @Nullable final String password);

    @Nullable void create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @Nullable void create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable void lockUserByLogin(
            @Nullable String currentLogin,
            @Nullable String login
    ) throws Exception;

    @Nullable void unlockUserByLogin(
            @Nullable String currentLogin,
            @Nullable String login
    ) throws Exception;

    void renamePassword(
            @Nullable String userId,
            @Nullable String newPassword
    ) throws Exception;

    void renameFirstName(
            @Nullable String currentUserId,
            @Nullable String newFirstName
    ) throws Exception;

}
